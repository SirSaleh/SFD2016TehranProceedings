\contentsline {chapter}{\numberline {1}ارائه‌ها}{3}{chapter.1}
\contentsline {section}{\numberline {1‏.‏1}اوپن‌استریت‌مپ (OSM) }{4}{section.1.1}
\contentsline {section}{\numberline {1‏.‏2} خلاقیت و دانش آزاد؛ ظهور طبقه‌ای جدید در جامعه}{5}{section.1.2}
\contentsline {section}{\numberline {1‏.‏3} سیاست‌های کمتر شناخته‌شدهٔ ویکی‌پدیا}{6}{section.1.3}
\contentsline {section}{\numberline {1‏.‏4} آزادی و محرمانگی در رایانش همراه}{7}{section.1.4}
\contentsline {section}{\numberline {1‏.‏5}چه هنگام نرم‌افزار آزاد به دام غیرآزاد می‌افتد؟}{8}{section.1.5}
\contentsline {section}{\numberline {1‏.‏6}با سوزان آشنا شوید! (معرفی نرم افزار بلندر)}{9}{section.1.6}
\contentsline {section}{\numberline {1‏.‏7} اقتصاد نرم‌افزار آزاد}{10}{section.1.7}
\contentsline {section}{\numberline {1‏.‏8}برنامهٔ آموزشی ویکی‌پدیا}{11}{section.1.8}
\contentsline {section}{\numberline {1‏.‏9} حفاظت از حریم خصوصی با تور}{13}{section.1.9}
\contentsline {section}{\numberline {1‏.‏10} \lr {StarCalendar}}{14}{section.1.10}
\contentsline {section}{\numberline {1‏.‏11}معرفی بنیاد ویکی‌مدیا و پروژه‌هایش (به جز ویکی‌پدیا)}{15}{section.1.11}
\contentsline {section}{\numberline {1.12}Rights in CopyLeft}{16}{section.1.12}
\contentsline {section}{\numberline {1‏.‏13} چرایی تغییر دیدگاه شرکت‌های انحصاری نسبت‌به نرم‌افزار آزاد}{17}{section.1.13}
\contentsline {section}{\numberline {1‏.‏14} اهمیت وب نامتمرکز و معرفی ابزار ZeroNet }{18}{section.1.14}
\contentsline {section}{\numberline {1‏.‏15} چرا اکثر ابررایانه‌ها از گنو/لینوکس استفاده می‌کنند؟}{19}{section.1.15}
\contentsline {section}{\numberline {1‏.‏16}کارمند دون‌پایه (چرا فلسفهٔ نرم افزار آزاد باعث بهبود عملکرد کارکنان می‌شود؟)}{20}{section.1.16}
\contentsline {section}{\numberline {1‏.‏17}حرکت شتابدار به سوی دنیای \lr {Embedded System} و نقش نرم‌افزارهای آزاد بر آن}{21}{section.1.17}
\contentsline {section}{\numberline {1‏.‏18} سخنرانی اختتامیه}{22}{section.1.18}
\contentsline {section}{\numberline {1‏.‏19}معرفی رایانش ابری و \lr {openstack}}{23}{section.1.19}
\contentsline {section}{\numberline {1‏.‏20} طراحی قالب جوملا}{24}{section.1.20}
\contentsline {section}{\numberline {1‏.‏21} برنامه‌نویسی به زبان آزاد دات‌نت در گنو/لینوکس}{25}{section.1.21}
\contentsline {section}{\numberline {1‏.‏22} پیاده‌سازی وب‌سرورهای فوق سریع}{26}{section.1.22}
\contentsline {section}{\numberline {1‏.‏23}کارگاه \lr {Raspberry Pi}}{27}{section.1.23}
\contentsline {section}{\numberline {1‏.‏24} ایجاد نرم‌افزارهای \lr {real-time} در \lr {PHP} به کمک \lr {WebSocket}}{28}{section.1.24}
\contentsline {section}{\numberline {1‏.‏25} معرفی و ساخت یک فریم‌ورک شخصی به کمک لاراول}{29}{section.1.25}
\contentsline {section}{\numberline {1‏.‏26} داده‌کاوی و زبان برنامه‌نویسی \texttt {R} }{30}{section.1.26}
\contentsline {section}{\numberline {1‏.‏27} کارگاه Inkscape }{32}{section.1.27}
\contentsline {section}{\numberline {1‏.‏28}کارگاه بلندر}{33}{section.1.28}
\contentsline {section}{\numberline {1‏.‏29} راه‌اندازی و کار با ابزار و شبکهٔ ZeroNet }{34}{section.1.29}
\contentsline {section}{\numberline {1‏.‏30} آموزش ویکی‌نویسی }{36}{section.1.30}
\contentsline {section}{\numberline {1‏.‏31} کار با اوپن‌استریت‌مپ \lr {(OSM)} از مبتدی تا پیشرفته }{37}{section.1.31}
\contentsline {section}{\numberline {1‏.‏32} کارگاه Jekyll }{38}{section.1.32}
\contentsline {section}{\numberline {1‏.‏33} چگونگی ارسال packet در شبکه و مروری بر Wireshark }{39}{section.1.33}
\contentsline {section}{\numberline {1.34} Real World Penetration Testing }{40}{section.1.34}
\contentsline {section}{\numberline {1‏.‏35} بررسی چالش کارکردهای بی‌درنگ در سیستم‌های نهفته }{41}{section.1.35}
\contentsline {section}{\numberline {1‏.‏36} مروری بر سخت‌افزارهای آزاد و نقش آن‌ها در سیستم‌های نهفته }{42}{section.1.36}
\contentsline {section}{\numberline {1‏.‏37} پردازش تصویر در سیستم‌های نهفتهٔ نوین با استفاده از بسترهای نرم‌افزاری آزاد }{43}{section.1.37}
\contentsline {section}{\numberline {1‏.‏38}قانون در بی‌قانونی (نگاهی بر ارز رمز پایه)}{44}{section.1.38}
\contentsline {section}{\numberline {1‏.‏39} مقایسه و معرفی راه‌کارها و ابزارهای مبتی بر تکنولوژی وب}{45}{section.1.39}
